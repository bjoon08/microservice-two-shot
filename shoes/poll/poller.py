import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

WARDROBE_BIN_API = "http://wardrobe-api:8000/api/bins/"


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            response = requests.get(WARDROBE_BIN_API)
            data = json.loads(response.content)
            bins = data["bins"]

            for bin in bins:
                BinVO.objects.update_or_create(
                    import_href=bin["href"],
                    defaults={
                        "closet_name": bin["closet_name"],
                        "bin_number": bin["bin_number"],
                        "bin_size": bin["bin_size"],
                    }
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(15)


if __name__ == "__main__":
    poll()
