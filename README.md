# Wardrobify

Team:

* Joon Hyuk Brandon Jang - Shoes microservice
* Santi Bothe - Hats microservice

## Design

The Wardrobify microservices are designed to be scalable, maintainable, and secure. They are built using the Django REST framework, which provides a set of tools for building RESTful APIs that can handle HTTP requests and responses in a standard way.

Each microservice is containerized using Docker, which allows for easy deployment and scaling of services across different environments.

The Shoes microservice includes a Views component that handles HTTP requests for listing, creating, and deleting shoes. It also includes encoders for displaying shoe lists, shoe details, and bin information. The Poller component interacts with the Wardrobe API using the requests library to fetch data and create BinVO objects based on the Bin ID. The React Shoes Route component is built using the React framework and enables creating, deleting, and listing shoes in a user-friendly manner.

The Hats microservice includes a Views component that handles HTTP requests for getting, posting, and deleting hats. It also includes encoders for encoding lists, details, and locations. The Poller component makes an API call to the Wardrobe API using requests and creates LocationVO objects based on the Location ID. The React Hats Route component enables creating, deleting, and listing hats in a user-friendly manner.

## Shoes microservice

Shoes Api:
-Views-
Handles HTTP requests for listing, creating, and deleting shoes.
It includes encoders for displaying shoe lists, shoe details, and bin information.

-Poller-
Interacts with the Wardrobe API using the requests library to fetch data and create BinVO objects based on the Bin ID.

-React Shoes Route-
Built using the React framework that enables creating, deleting, and listing shoes.
It interfaces with the Views component to perform these actions in a user-friendly manner.

## Hats microservice

Hats Api-
  Views
    - Get, Post, Delete
    - list encoder, detail encoder, location encoder
  Poller
    - Makes an API call using requests to Wardrobe API.
    - makes LocationVO objects based on Location id
  React Hats Route
    - Create, Delete, and List
