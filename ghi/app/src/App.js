import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Shoes from './shoes_components/ShoePage';

import HatPage from "./hats_components/HatPage"
import React, { useState, useEffect } from 'react';


function App(props) {


  const [hats, setHats] = useState([]);
  const [locations, setLocations] = useState([]);


  async function fetchHatData() {
    const hatsUrl = "http://localhost:8090/api/hats/";
    const response = await fetch(hatsUrl);
    if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      }
    }

  async function fetchLocationData() {
    const hatsUrl = "http://localhost:8100/api/locations/";
    const response = await fetch(hatsUrl);
    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        console.log(data.locations)
      }
    }

  useEffect(() => {
    fetchHatData()
    fetchLocationData()
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage hats={hats} />} />
          <Route path="hats/" element={<HatPage hats={hats} locations={locations} fetchHatData={fetchHatData} />} />
          <Route path="/shoes" element={<Shoes />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
