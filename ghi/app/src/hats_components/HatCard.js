import Delete from "./DeleteHat"

const HatCard = ({hats, fetchHatData}) => {

  return (
    <>
    {hats.map( hat => {
      console.log(hat.id)
      return (
      <div className="col-sm-12 col-md-6 col-lg-3" key={hat.id}>
        <div className="card h-100 w-100" >
        <img src={hat.picture_url} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{hat.style_name}</h5>
          <h6 className="card-subtitle text-muted">{hat.location.closet_name}</h6>
          <p className="card-text">{hat.fabric} | {hat.color}</p>
        </div>
        <div className="card-footer">
          <Delete id={hat.id} fetchHatData={fetchHatData}/>
        </div>
      </div>
    </div>
      )
      })}
    </>
  )
}

export default HatCard;
