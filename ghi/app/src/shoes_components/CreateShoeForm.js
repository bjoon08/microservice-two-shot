import { useEffect, useState } from "react";

const CreateShoeForm = ({bins, fetchShoeData}) => {

    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturer = (event) => {
        setManufacturer(event.target.value);
    }
    const [modelName, setModelName] = useState('');
    const handleName = (event) => {
        setModelName(event.target.value);
    }
    const [color, setColor] = useState('');
    const handleColor = (event) => {
        setColor(event.target.value);
    }
    const [picture, setPicture] = useState('');
    const handlePicture = (event) => {
        setPicture(event.target.value);
    }
    const [bin, setBin] = useState('');
    const handleBin = (event) => {
        setBin(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfigUrl = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(shoeUrl, fetchConfigUrl);

        if (response.ok) {

            setManufacturer('')
            setModelName('')
            setColor('')
            setPicture('')
            setBin('')
        }
        window.location.reload();
    }

    return (
        <div className="modal fade" id="createshoe" tabIndex="-1" aria-labelledby="createshoeLabel" aria-hidden="true">
            <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="createshoeLabel">Create a new shoe</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="manufacturer" className="col-form-label">Manufacturer</label>
                    <input value={manufacturer} onChange={handleManufacturer} type="text" className="form-control" id="manufacturer" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="modelname" className="col-form-label">Model Name</label>
                    <input value={modelName} onChange={handleName} type="text" className="form-control" id="modelname" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="color" className="col-form-label">Color</label>
                    <input value={color} onChange={handleColor} type="text" className="form-control" id="color" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="pictureurl" className="col-form-label">Picture url</label>
                    <input value={picture} onChange={handlePicture} className="form-control" id="pictureurl"></input>
                    </div>
                    <div className="mb-3">
                    <select value={bin} onChange={handleBin} className="form-select" aria-label="Default select example">
                        <option value="">Open this select menu</option>
                        {bins?.map(bin => {
                        return (
                            <option value={bin.href} key={bin.href}>{bin.closet_name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <div className="modal-footer">
                    <button data-bs-dismiss="modal" type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    )
}

export default CreateShoeForm;
