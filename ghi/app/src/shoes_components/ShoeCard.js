import Delete from "./DeleteShoe";

const ShoeCard = ({ shoes, fetchShoeData }) => {
    return (
        <>
        {shoes.map(shoe => {
            return (
            <div className="col-4" key={shoe.id}>
                <div className="card h-100 w-100">
                <img src={shoe.picture_url} className="card-img-top" />
                <div className="card-body">
                    <h5 className="card-title">{shoe.model_name}</h5>
                    <p className="card-text">
                    {shoe.manufacturer} | {shoe.color}
                    </p>
                </div>
                <div className="card-footer">{shoe.bin.closet_name}</div>
                <div className="card-footer">
                    <Delete id={shoe.id} fetchShoeData={fetchShoeData} />
                </div>
                </div>
            </div>
            );
        })}
        </>
    );
};

export default ShoeCard;
