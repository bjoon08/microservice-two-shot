import React, { useEffect, useState } from 'react';
import ShoeCard from "./ShoeCard";
import CreateShoeForm from './CreateShoeForm';

const Shoes = () => {
    const [bins, setBins] = useState([]);
    const [shoes, setShoes] = useState([]);

    async function fetchBinData() {
        const shoesUrl = 'http://localhost:8100/api/bins';
        const response = await fetch(shoesUrl);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    async function fetchShoeData() {
        const shoesUrl = 'http://localhost:8080/api/shoes'
        const response = await fetch(shoesUrl)
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchBinData()
        fetchShoeData()
    }, []);

    return (
        <>
        <br />
        <div className="container" >
            <div className="row justify-content-end">
                <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createshoe" data-bs-whatever="@mdo">Create a Shoe</button>
            </div>
        </div>
        <CreateShoeForm fetchShoeData={fetchShoeData} shoes={shoes} bins={bins} />

        <div className="row gy-3">
            <ShoeCard shoes={shoes} />
        </div>
    </>
    );
}

export default Shoes;
