import { Link } from 'react-router-dom';

function MainPage() {

  const hatsPic = "https://img.thesitebase.net/10126/10126771/products/ver_13431054d54f891f6e3ec607301fc9488/0x720@16661495731e846804a6.jpeg"
  const shoesPic = "https://img.thesitebase.net/10266/10266415/products/ver_1/0x720@1643327684072b3ede53.jpeg"

  return (
    <>
    <div className="px-4 py-1 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">

        <div class="col-3">
        <div class="w-100 card">
          <img src={shoesPic} class="card-img-top" alt="..." />
          <div class="card-body">
            <Link to="/shoes" className="w-100 btn btn-info btn-lg">View Shoes</Link>
          </div>
        </div>
        </div>

        <div class="col-3">
        <div class="w-100 card">
          <img src={hatsPic} class="card-img-top" alt="..." />
          <div class="card-body">
          <Link to="/hats" className="w-100 btn btn-info btn-lg">View Hats</Link>
          </div>
        </div>
        </div>

        </div>
    </div>

    </>
  );
}

export default MainPage;
